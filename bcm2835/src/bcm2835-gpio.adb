with Interfaces; use Interfaces;
package body Bcm2835.gpio is
   procedure SetFunction
     (pin : Thin.RPiGPIOPin; fs : Thin.bcm2835FunctionSelect)
   is
   begin
      Thin.gpio_fsel (Unsigned_8 (pin), Unsigned_8 (fs));
   end SetFunction;
   function GetState (pin : Thin.RPiGPIOPin) return State is
   begin
      if Thin.LOW = Thin.gpio_lev (Unsigned_8 (pin)) then
         return Low;
      else
         return High;
      end if;
   end GetState;

   procedure SetState (pin : Thin.RPiGPIOPin; to : State) is
   begin
      if to = Low then
         Thin.gpio_write (Unsigned_8 (pin), Thin.LOW);
      else
         Thin.gpio_write (Unsigned_8 (pin), Thin.HIGH);
      end if;
   end SetState;

   procedure ToggleState (pin : Thin.RPiGPIOPin) is
   begin
      if GetState (pin) = Low then
         SetHigh (pin);
      else
         SetLow (pin);
      end if;
   end ToggleState;

   procedure SetHigh (pin : Thin.RPiGPIOPin) is
   begin
      SetState (pin, High);
   end SetHigh;

   procedure SetLow (pin : Thin.RPiGPIOPin) is
   begin
      SetState (pin, Low);
   end SetLow;

end Bcm2835.gpio;
