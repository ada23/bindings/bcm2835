with Bcm2835.Thin;
package Bcm2835.GPIO is
   package Thin renames Bcm2835.Thin;
   type State is (Low, High);
   procedure SetFunction
     (pin : Thin.RPiGPIOPin; fs : Thin.bcm2835FunctionSelect);
   function GetState (pin : Thin.RPiGPIOPin) return State;
   procedure SetState (pin : Thin.RPiGPIOPin; to : State);
   procedure ToggleState (pin : Thin.RPiGPIOPin);
   procedure SetHigh (pin : Thin.RPiGPIOPin);
   procedure SetLow (pin : Thin.RPiGPIOPin);
end Bcm2835.GPIO;
