with Interfaces.C; use Interfaces.C;
with Bcm2835.Thin;
package body bcm2835 is
   procedure MilliDelay (millisecs : Interfaces.C.unsigned) is
   begin
      Thin.rt_delay (millisecs);
   end MilliDelay;

   procedure MicroDelay (microsecs : Unsigned_64) is
   begin
      Thin.delayMicroseconds (microsecs);
   end MicroDelay;

begin
   if Thin.init /= 0 then
      raise Program_Error with "bcm2835 init failure";
   end if;
end bcm2835;
