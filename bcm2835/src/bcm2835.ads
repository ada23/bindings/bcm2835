with Interfaces; use Interfaces;
with Interfaces.C;

package Bcm2835 is
   pragma Elaborate_Body;
   procedure MilliDelay (millisecs : Interfaces.C.unsigned);
   procedure MicroDelay (microsecs : Unsigned_64);
end Bcm2835;
